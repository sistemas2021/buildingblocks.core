using System;
using System.Threading.Tasks;

namespace BuildingBlocks.Core.src.Interfaces.Data
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> Commit();
    }
}