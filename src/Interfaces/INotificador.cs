using System.Collections.Generic;
using BuildingBlocks.Core.src.Notificacoes;

namespace BuildingBlocks.Core.src.Interfaces
{
    
    public interface INotificador
    {
        bool TemNotificacao();
        List<Notificacao> ObterNotificacoes();
        void Handle(Notificacao notificacao);
    }
}