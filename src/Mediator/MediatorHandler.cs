﻿using FluentValidation.Results;
using System.Threading.Tasks;
using MediatR;
using BuildingBlocks.Core.src.Messages;
using Microsoft.EntityFrameworkCore;
using BuildingBlocks.Core.src.DomainObjects;
using System.Linq;

namespace BuildingBlocks.Core.src.Mediator
{
    public class MediatorHandler : IMediatorHandler
    {
        private readonly IMediator _mediator;

        public MediatorHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<ValidationResult> EnviarComando<T>(T comando) where T : Command
        {
            return await _mediator.Send(comando);
        }

        public async Task PublicarEvento<T>(T evento) where T : Event
        {
            await _mediator.Publish(evento);
        }

        public async Task PublicarEventos<T>(T context) where T : DbContext
        {
            var domainEntities = context.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.Notificacoes != null && x.Entity.Notificacoes.Any());

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.Notificacoes)
                .ToList();

            domainEntities.ToList()
                .ForEach(entity => entity.Entity.LimparEventos());

            var tasks = domainEvents
                .Select(async (domainEvent) => {
                    await PublicarEvento(domainEvent);
                });

            await Task.WhenAll(tasks);
        }
    }
}