﻿using System.Threading.Tasks;
using BuildingBlocks.Core.src.Messages;
using FluentValidation.Results;
using Microsoft.EntityFrameworkCore;

namespace BuildingBlocks.Core.src.Mediator
{
    public interface IMediatorHandler
    {
        Task PublicarEvento<T>(T evento) where T : Event;
        Task PublicarEventos<T>(T context) where T : DbContext;
        Task<ValidationResult> EnviarComando<T>(T comando) where T : Command;
    }
}