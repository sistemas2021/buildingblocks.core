using BuildingBlocks.Core.src.DomainObjects;
using Xunit;

namespace BuildingBlocks.Core.Tests.DomainObjects
{
    public class EmailTests
    {
        [Trait("Categoria","Email")]
        [Theory(DisplayName = "Validar Email válidos")]
        [InlineData("teste@teste.com")]
        [InlineData("123@abc.com")]
        public void ValidarEmail_EmailValido_TodosDevemSerValidos(string Email)
        {
            // Assert
            var EmailValidacao = new Email(Email);

            // Act & Assert
            Assert.Equal(Email, EmailValidacao.Endereco);
        }

        [Trait("Categoria","Email")]
        [Theory(DisplayName = "Validar Email inválidos")]
        [InlineData("teste.com")]
        [InlineData("a@a")]
        public void ValidarEmail_EmailInvalido_TodosDevemSerInvalidos(string Email)
        {
            // Arrange & Act & Assert
            Assert.Throws<DomainException>(() => new Email(Email));
        }
    }
}