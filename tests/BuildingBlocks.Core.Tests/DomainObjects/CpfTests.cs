using BuildingBlocks.Core.src.DomainObjects;
using Xunit;

namespace BuildingBlocks.Core.Tests.DomainObjects
{
    public class CpfTests
    {
        [Trait("Categoria","CPF")]
        [Theory(DisplayName = "Validar cpf válidos")]
        [InlineData("15231766607")]
        [InlineData("78246847333")]
        [InlineData("64184957307")]
        [InlineData("21681764423")]
        [InlineData("13830803800")]
        [InlineData("04774583944")]
        [InlineData("4774583944")]
        public void ValidarCpf_CpfValido_TodosDevemSerValidos(string cpf)
        {
            // Assert
            var cpfValidacao = new Cpf(cpf);

            // Act & Assert
            Assert.Equal(cpf, cpfValidacao.Numero);
        }

        [Trait("Categoria","CPF")]
        [Theory(DisplayName = "Validar cpf inválidos")]
        [InlineData("15231766607213")]
        [InlineData("528781682082")]
        [InlineData("35555868512")]
        [InlineData("36014132822")]
        [InlineData("72186126500")]
        [InlineData("23775274811")]
        [InlineData("12345678909")]        
        public void ValidarCpf_CpfInvalido_TodosDevemSerInvalidos(string cpf)
        {
            // Arrange & Act & Assert
            Assert.Throws<DomainException>(() => new Cpf(cpf));
        }
    }
}