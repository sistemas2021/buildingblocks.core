using BuildingBlocks.Core.src.DomainObjects;
using Xunit;

namespace BuildingBlocks.Core.Tests.DomainObjects
{
    public class CnpjTests
    {
        [Trait("Categoria","CNPJ")]
        [Theory(DisplayName = "Validar cnpj válidos")]
        [InlineData("02828446000134")]
        [InlineData("72610132000146")]
        [InlineData("01874354000128")]
        [InlineData("04220692000134")]
        [InlineData("26313061000100")]
        [InlineData("28284460001341")]
        public void ValidarCnpj_CnpjValido_TodosDevemSerValidos(string Cnpj)
        {
            // Arrange
            var CnpjValidacao = new Cnpj(Cnpj);

            // Act & Assert
            Assert.Equal(Cnpj, CnpjValidacao.Numero);
        }

        [Trait("Categoria","CNPJ")]
        [Theory(DisplayName = "Validar cnpj inválidos")]
        [InlineData("28284460001345")]
        [InlineData("28284460001343")]
        [InlineData("28284460001342")]
        [InlineData("7261013200014")]
        [InlineData("01874354000129")]
        [InlineData("04220692000100")]
        [InlineData("16159125000152")]
        public void ValidarCnpj_CnpjInvalido_TodosDevemSerInvalidos(string Cnpj)
        {
            // Arrange & Act & Assert
            Assert.Throws<DomainException>(() => new Cnpj(Cnpj));
        }
    }
}
