using BuildingBlocks.Core.src.Utils;
using Xunit;

namespace BuildingBlocks.Core.Tests.Utils
{
    public class StringUtilsTests
    {
        [Theory(DisplayName = "Retornar todo texto")]
        [Trait("Categoria","String")]
        [InlineData("02828446000134")]
        [InlineData("72610132000146")]
        [InlineData("01874354000128")]
        [InlineData("04220692000134")]
        [InlineData("26313061000100")]
        [InlineData("28284460001341")]
        public void ApenasNumeros_StringSoComNumeros_DeveRetornarStringToda(string valor)
        {
            // Arrange & Act
            var texto = valor.ApenasNumeros(valor);           
        
            // Assert
            Assert.Equal(valor, texto);
        }

        [Theory(DisplayName = "Retornar apenas os números")]
        [Trait("Categoria","String")]
        [InlineData("0a2d8f2g8h4g4dfg6hj0j0k0f1hj34", "02828446000134")]
        [InlineData("72asdf6hcv1n0u13fgjl2h0sdfg00as146","72610132000146")]
        [InlineData("01!@8%74¨&3@!54!@#00¨&01*28","01874354000128")]
        public void ApenasNumeros_StringSoMisturado_DeveRetornarApenasNumeros(string valor, string resultado)
        {
            // Arrange & Act
            var texto = valor.ApenasNumeros(valor);           
        
            // Assert
            Assert.Equal(resultado, texto);
        }        
    }
}